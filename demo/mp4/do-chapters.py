#!/bin/env python3

import re

def read_chapters(filename):
    print("Read chapters from {}...".format(filename), sep=" ", end="")

    with open(filename, 'r') as f:
        chapters = list()
        for line in f:
            try:
                x = re.match(r"(\d{2}):(\d{2}) (.*)", line)
            except:
                print("Error: excepted format 'XY:uv The title'")
                return

            mins = int(x.group(1))
            secs = int(x.group(2))
            title = x.group(3)

            minutes = mins
            seconds = secs + (minutes * 60)
            timestamp = (seconds * 1000)
            chap = {
                "title": title,
                "time": timestamp
            }
            chapters.append(chap)

    print("done.")
    return chapters


def generate_meta(chapters, offset=0):
    print("Generate meta...", sep=" ", end="")

    text = ""
    for i in range(len(chapters)-1): # Require unnecessary mm:ss Stop
        chap = chapters[i]
        title = chap['title']
        start = chap['time'] + offset
        end = chapters[i+1]['time']-1 + offset
        text += f"""
[CHAPTER]
TIMEBASE=1/1000
START={start}
END={end}
title={title}
"""

    print("done.")
    return text, end

if __name__ == "__main__":
    import sys
    try:
        offset = 0
        for i, filename in enumerate(sys.argv[1:]):
            chapters= read_chapters(filename)
            text, offset = generate_meta(chapters, offset)
            if len(sys.argv) > 2:
                out, perm = "merged.meta", 'a'
                if i == 0:
                    with open(out, 'w') as f:
                        f.write('')
            else:
                out, perm = filename + '.meta', 'w'
            with open(out, perm) as f:
                f.write(text)
            print("Wrote {}".format(out))
    except:
        print("Error.")
        sys.exit(1)
