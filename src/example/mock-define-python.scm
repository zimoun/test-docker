(define python
  (package
    (name "python")
    (version "3.9.9")
    (source ... )
    (build-system gnu-build-system)
    (arguments ... )
    (inputs (list bzip2 expat gdbm libffi sqlite
                  openssl readline zlib tcl tk))))
